import boto3
import os
from get_servers import get_servers
from get_connections import get_active_streams
from botocore.config import Config
import datetime
from dateutil.tz import tzutc
from kill_server import detach_and_kill_server


config = Config(
    region_name = os.environ.get("AWS_DEFAULT_REGION"),
    signature_version = 'v4',

    retries = {
        'max_attempts': 5,
        'mode': 'standard'
    }
)

service = os.environ.get("SERVICE", "ec2")
autoscale_group = os.environ.get("AUTOSCALE_GROUP", "flussonic-autoscaling")
parameter = os.environ.get("PARAMETER", "PublicDnsName")

login = os.environ.get("FLUSSONIC_LOGIN")
password = os.environ.get("FLUSSONIC_PASSWORD")

time_to_die = int(os.environ.get("TIME_TO_DIE", 15))

servers = get_servers(service, autoscale_group, parameter)


cloudwatch = boto3.client('cloudwatch')

for index, serv in enumerate(servers):
    connections = get_active_streams(login, password, serv)
    print(f"serving {serv}")
    
    if type(connections) is int:
    # uncomment to create new metrics for every server
    #     cloudwatch.put_metric_data(
    #         MetricData=[
    #             {
    #                 'MetricName': f'Instance-{serv["id"]}-active-connections',
    #                 'Dimensions': [
    #                     {
    #                         'Name': 'Instance',
    #                         'Value': serv['id'],

    #                     },
    #                 ],
    #                 'Unit': 'None',
    #                 'Value': connections
    #             },
    #         ],
    #         Namespace = 'Custom'
    #     )
        print(f'Server {serv} has {connections} connections')

        # Check how old is server if there is 0 streams on it
       
        if connections == 0 and len(servers) > 1:
            time = datetime.datetime.now()
            diff = time - serv['creation'].replace(tzinfo=None)
            if diff.seconds/60 > time_to_die:
                #if server older than 15 minutes then detach him from autoscale and kill
                detach_and_kill_server(serv['id'], autoscale_group)
                del servers[index]

    else:
        print(connections)








