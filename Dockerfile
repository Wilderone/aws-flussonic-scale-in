FROM ubuntu:20.10

RUN apt update && apt install -y curl unzip python3 pip
RUN mkdir /app && mkdir /root/.aws

WORKDIR /app

RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install

RUN echo "[default]" > ~/.aws/config && \
    echo "region=${AWS_DEFAULT_REGION}" >> ~/.aws/config && \
    echo "output=json" >> ~/.aws/config

RUN echo "[default]" > ~/.aws/credentials && \
    echo "aws_access_key_id=${AWS_ACCESS_KEY_ID}" >>  ~/.aws/credentials && \
    echo "aws_secret_access_key=${AWS_SECRET_ACCESS_KEY}" >>  ~/.aws/credentials

COPY ./ ./

RUN pip install -r requirements.txt
RUN chmod 0744 ./main.py
RUN chmod 0764 ./run-scheduler.sh


CMD ./run-scheduler.sh

