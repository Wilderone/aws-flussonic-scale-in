#!/usr/bin/env sh
export $(cat .env | xargs)
while [ true ]
do
  python3 main.py &
  sleep 300
done