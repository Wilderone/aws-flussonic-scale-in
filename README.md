This script check flussonic server in autoscale group for connections.
If there is no connections for last $TIME_TI_DIE minutes (15 by default)
Server will be detached from autoscale group and terminated, if this is 
not last server of flussonic.

Script use this environment variables:
# aws cli config to authorize in aws services.
AWS_DEFAULT_REGION
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY

# instance parameters (defaults ec2, flussonic-autoscaling, PublicDnsName)
SERVICE
AUTOSCALE_GROUP
PARAMETER

# flussonic credentials
FLUSSONIC_LOGIN
FLUSSONIC_PASSWORD

# uptime of server to terminate if there is 0 streams. (default 15 min)
TIME_TO_DIE

# Requirements:
aws-cli
python 3.7.3
