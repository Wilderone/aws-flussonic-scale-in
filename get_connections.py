import boto3 
import os
from botocore.config import Config
import requests
import json



config = Config(
    region_name = os.environ.get("AWS_DEFAULT_REGION"),
    signature_version = 'v4',

    retries = {
        'max_attempts': 5,
        'mode': 'standard'
    }
)

def get_active_streams(login, password, server):

    """
        This use return of get_service for ask flussonic servers
        about active streams. Return number of active streams 
    """


   
    active_streams = 0
    if server.items():
        try:
            response = json.loads(requests.get(f'http://{server["dns"]}/flussonic/api/media', auth=(login, password)).content)
        except requests.exceptions.ConnectionError:
            return f"{server} still initializing"
        for stream in response:
            if stream['value']['stats']['alive']:
                active_streams += 1
  
    return active_streams

if __name__ == '__main__':

    get_active_streams()