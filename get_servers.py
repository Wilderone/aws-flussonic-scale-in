
import boto3 
import os
from botocore.config import Config




def get_servers(service, sg_name, parameter):
    """
    Get value of PARAMETER of SERVICE objects with SG_NAME - autoscaling group name 
    """
    ec2 = boto3.client(service)
    response = ec2.describe_instances()
    flussonic_servers = []
    for reserv in response['Reservations']:
        for instance in reserv['Instances']:
            # pprint(instance)
            if instance['State']['Code'] == 16: # 16 is code for running instance state
                for tag in instance['Tags']:
                    if sg_name in tag.values():
                        try:
                            flussonic_servers.append({
                                "id": instance['InstanceId'],
                                "dns": instance[parameter],
                                "creation": instance['LaunchTime']
                                })
                        except KeyError:
                            print('KeyError raised')
                            break                 

    return flussonic_servers


if __name__ == '__main__':
    service = os.environ.get("SERVICE", "ec2")
    autoscale_group = os.environ.get("AUTOSCALE_GROUP", "flussonic-autoscaling")
    parameter = os.environ.get("PARAMETER", "PublicDnsName")
    get_servers(service, autoscale_group, parameter)