import boto3 
import os
from botocore.config import Config
from pprint import pprint
from get_servers import get_servers


def kill_server(id):
    #Terminate instance
    client = boto3.client('ec2')
    
    response = client.terminate_instances(
    InstanceIds=[
        str(id),
    ],
    DryRun=False
    )
    print(f"\n**** Instance {id} is terminated *****\n")
    print(f"\nresponse after TERMINATING server {response} \n")

def detach_and_kill_server(id, group_name):
    #Detach server from autoscale group and terminate server
   
    client = boto3.client('autoscaling')
    try:
        
        response = client.detach_instances(
            InstanceIds=[
                str(id),
            ],
            AutoScalingGroupName=str(group_name),
            ShouldDecrementDesiredCapacity=True
        )
        print(f"\nServer {id} detached from scaling group\n")
        
        kill_server(id)

    except client.exceptions.ResourceContentionFault as err:
        print('ResourceContentionFault was rised \n', err)
        print(f'response is {response}')
    

